int sensorPin = A5;
int buttonPlus = A3;
int buttonLess = A4;
int led1 = 2;
int led2 = 3;
int led3 = 9;
int led4 = 10;
int led5 = 11;

// Create a variable to hold the light reading
int sensorValue;
float degreesC = 0;
float coef = 0.00357;
//float coef = 0.0026;

void setup()
{
  // Set sensorPin as an INPUT
  pinMode(sensorPin, INPUT);
  pinMode(buttonPlus, INPUT_PULLUP);   
  pinMode(buttonLess, INPUT_PULLUP);   
  
  // this is the LED that flashes up when we reach x C and more
  pinMode(led1, OUTPUT);  
  
  // Initialize Serial, set the baud rate to 9600 bps.
  Serial.begin(9600);

  Serial.print("Initial value for coef x 1000: ");
  Serial.println(coef*1000);
}

float sumXread(int num)
{
    // read sensor num times and sum values
    float total = 0; // reset value
    for (int x = 0; x < num; x++) { // read sensor num times 
          sensorValue = analogRead(sensorPin);
          total = total + sensorValue; // add each value to a total
          //Serial.print("Sensor: ");
          //Serial.print(sensorValue);  
          //Serial.print(" Total : ");
          //Serial.print(total);
          //Serial.print(" Average : ");
          //Serial.println(total / (x+1));
    }
    return total;
}

void displayLed()
{
    float total = sumXread(10);
    degreesC = (((total / 10) * coef) - 0.5) * 100;
    Serial.print("1000 coef = ");
    Serial.print(coef * 1000);
    Serial.print(" total = ");
    Serial.print(total);
    Serial.print(" degrees C = ");
    Serial.println(degreesC);

    if (degreesC >= 10 && degreesC < 15) {
      Serial.println("LED1 only");
      digitalWrite(led1, HIGH);
      digitalWrite(led2, LOW);
      digitalWrite(led3, LOW);
      digitalWrite(led4, LOW);
      digitalWrite(led5, LOW);
    }

    if (degreesC >= 15 && degreesC < 20) {
      Serial.println("LED1 & LED2");
      digitalWrite(led1, HIGH);
      digitalWrite(led2, HIGH);
      digitalWrite(led3, LOW);
      digitalWrite(led4, LOW);
      digitalWrite(led5, LOW);
    }

    if (degreesC >= 20 && degreesC < 25) {
      Serial.println("LED1 & LED2 & LED3");
      digitalWrite(led1, HIGH);
      digitalWrite(led2, HIGH);
      digitalWrite(led3, HIGH);
      digitalWrite(led4, LOW);
      digitalWrite(led5, LOW);
    }

    if (degreesC >= 25 && degreesC < 30) {
      Serial.println("LED1 & LED2 & LED3 & LED4");
      digitalWrite(led1, HIGH);
      digitalWrite(led2, HIGH);
      digitalWrite(led3, HIGH);
      digitalWrite(led4, HIGH);
      digitalWrite(led5, LOW);
    }

    if (degreesC >= 30) {
      Serial.println("All leds");
      digitalWrite(led1, HIGH);
      digitalWrite(led2, HIGH);
      digitalWrite(led3, HIGH);
      digitalWrite(led4, HIGH);
      digitalWrite(led5, HIGH);
    }
}


int isButtonPlusPressed() {
 int buttonP = digitalRead(buttonPlus);
 if (buttonP == LOW) {
    Serial.println("Button + is pressed ");
    return 1;
  } else {
    Serial.println("Button + is NOT pressed");
    return 0;
  }
}

int isButtonLessPressed() {
  int buttonL = digitalRead(buttonLess);
   if (buttonL == LOW) {
    Serial.println("Button - is pressed ");
    return 1;
  } else {
    Serial.println("Button - is NOT pressed");
    return 0;
  }
}

void blinkLeds() {
  Serial.println("Blinking all leds");
  digitalWrite(led1, LOW);
  digitalWrite(led2, LOW);
  digitalWrite(led3, LOW);
  digitalWrite(led4, LOW);
  digitalWrite(led5, LOW);
  delay(100);
  digitalWrite(led1, HIGH);
  digitalWrite(led2, HIGH);
  digitalWrite(led3, HIGH);
  digitalWrite(led4, HIGH);
  digitalWrite(led5, HIGH);
  delay(100);
}

void testLed() {
  digitalWrite(led1, LOW);
  digitalWrite(led2, LOW);
  digitalWrite(led3, LOW);
  digitalWrite(led4, LOW);
  digitalWrite(led5, LOW);
  delay(400);
  Serial.println("Lighting up LED1");
  digitalWrite(led1, HIGH);
  delay(400);
  Serial.println("Lighting up LED2");
  digitalWrite(led2, HIGH);
  delay(400);
  Serial.println("Lighting up LED3");
  digitalWrite(led3, HIGH);
  delay(400);
  Serial.println("Lighting up LED4");
  digitalWrite(led4, HIGH);
  delay(400);
  Serial.println("Lighting up LED5");
  digitalWrite(led5, HIGH);
}



void loop()
{

  //testLed();
  
  displayLed();
  
  if (isButtonPlusPressed()) {
    blinkLeds(); // blink twice to indicate we received the button press
    blinkLeds();
    coef = coef + 0.00005;
  }
  if (isButtonLessPressed()) {
    blinkLeds(); // blink twice to indicate we received the button press
    blinkLeds();
    coef = coef - 0.00005;
  }
  // Delay so that the text doesn't scroll too fast on the Serial Monitor. 
  // Adjust to a larger number for a slower scroll.
  delay(1000);
}

